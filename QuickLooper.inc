#!/bin/env -S -i bash --norc

"${common_lib_loaded:-false}" && return

source "${0}.cfg" # Main configuration file

do_initial_configuration () {
    # Hardcoded values when no configuration is available (TODO: Generate a default configuration file. Maybe use a more user friendly format than a shell script designed to be sourced)
    [[ "${QL_REPETITIONS}" =~ ^[0-9]+$ ]] || QL_REPETITIONS="2"
    [[ "${QL_TEMPO}" =~ ^[0-9]+$       ]] || QL_TEMPO="60"
    [[ "${QL_AUDIO_OUTPUT}"            ]] || QL_AUDIO_OUTPUT="alsa" # TODO: better test of default value
    [[ "${QL_AUDIO_NORMALIZATION}"     ]] || QL_AUDIO_NORMALIZATION="-6"
    [[ "${QL_REGEX_TYPE}"              ]] || QL_REGEX_TYPE="egrep"
    [[ "${QL_SAMPLE_MIN_SIZE}"         ]] || QL_SAMPLE_MIN_SIZE="500k"
    [[ "${QL_SAMPLE_MAX_SIZE}"         ]] || QL_SAMPLE_MAX_SIZE="10M"
    [[ "${QL_FILE_EXTENSIONS}"         ]] || QL_FILE_EXTENSIONS="flac"
    [[ "${QL_TMPFILE_DIRECTORY}"       ]] || QL_TMPFILE_DIRECTORY="/tmp"
    [[ "${QL_SOX_INVOCATION}"          ]] || QL_SOX_INVOCATION="/usr/bin/sox -q"
    [[ "${QL_BC_INVOCATION}"           ]] || QL_BC_INVOCATION="/usr/bin/bc -q"
    [[ "${QL_BC_SCALE}"                ]] || QL_BC_SCALE='6'
    [[ "${QL_SOXLOG_DIRECTORY}"        ]] || QL_SOXLOG_DIRECTORY='./QLReplay'
    [[ "${QL_RANDOMIZE_PITCH}"         ]] || QL_RANDOMIZE_PITCH='0'
}

stderr () { printf "%-6i %b\n" "${BASHPID}" "${1}" >&2; }

declare -g  sox_audio_output="-t ${QL_AUDIO_OUTPUT}"

if [[ ! "${QL_AUDIO_NORMALIZATION}" =~ ^-?[0-9]+$ ]]; then
    declare -g normalization=''
    stderr "Global normalization not active."
else
    declare -g normalization="--norm=${QL_AUDIO_NORMALIZATION}"
    stderr "Global normalization active (${QL_AUDIO_NORMALIZATION} dB)"
fi
declare -g  play_mode
declare -gr sox_command_prefix="${QL_SOX_INVOCATION} -G ${normalization}"
declare -gi repetition
declare -ga melody_elements
declare -g  randomize_pitch="${QL_RANDOMIZE_PITCH}"
declare -g  beat_length
declare -g  audio_file
declare -g  audio_length
declare -g  pitch_bend_start
declare -g  pitch_bend_end
declare -g  pitch_bend_amount
declare -g  pad_preloop_length
declare -g  pad_postloop_length
declare -g  note_pad_length
declare -g  note_start_position
declare -g  note_length
declare -g  note_on_length
declare -g  fade_in_length
declare -g  fade_out_length
declare -g  swoosh_length
declare -g  pre_stretch_pad_length
declare -g  play_pipe
declare -g  play_id
declare -g  total_loop_length
declare -g  regex

now () { date +%s%N; }
now_ms () { cut --complement -c 14- <(now); }
declare -gr my_name="$(basename "${0}")_$(now)"
do_initial_configuration
declare -gr session_id="${PPID}"
declare -gr session_pipe="${QL_TMPFILE_DIRECTORY}/${my_name}_${BASHPID}_sox-${session_id}"
declare -gr bc_pipe="${QL_TMPFILE_DIRECTORY}/${my_name}_bc-${session_id}"

init_bc () {
    [[ -p "${bc_pipe}" ]] || mkfifo "${bc_pipe}"
    exec 5> >(exec ${QL_BC_INVOCATION} <(echo "scale=${QL_BC_SCALE};") >"${bc_pipe}")
    exec 6<"${bc_pipe}"
}

init_bc

bc () {
    echo >&5 "${1}"
    read -u 6 x
    echo "${x}"
}

bc0 () {
    echo >&5 "scale=0;${1};scale=${QL_BC_SCALE}"
    read -u 6 x
    echo "${x}"
}

ran ()    { tr -cd "${1:-0-9}" < /dev/urandom > >(head -c"${2:-1}"); }
rmm ()    { local candidate="$(ran 0-9 6)"; bc0 "${1}+(${candidate}%${2})"; }
sec2ms () { cut -c 1-3 <(echo "${1#*.}"); }

loop_info () {
    printf "${@}" >&2
}

swoosh_info () {
    printf "${@}" >&2
}

choose_random_file () {
    [[ -d "${1}"  ]] || { stderr "“${1}” is not a directory.";   exit 4; }
    [[ "${2}" ]] || { stderr "“${2}” is not a valid regex."; exit 5; } # TODO: better test
    local directory="${1}"
    local regex="${2}"
    local extensions="$(tr ',' '|' < <(echo "${QL_FILE_EXTENSIONS}"))"
    find -L "${directory}" -size -"${QL_SAMPLE_MAX_SIZE}" -a -size +"${QL_SAMPLE_MIN_SIZE}" -a -regextype "${QL_REGEX_TYPE}" -iregex '^.*'"${regex}"'.*\.('"${extensions}"')$' -print | shuf -n1
}

wait_next_beat () { #1: beat_length, note_length or any other time period
    [[ "${1}" =~ ^\.?0+$ ]] && { echo 0; return; }
    elapsed_ms="$(($(now_ms)%$(sec2ms ${1})))"
    local time_to_wait="$(bc "scale=${QL_BC_SCALE};${1}-(${elapsed_ms}/1000)")"
    echo "${time_to_wait}"
}

collect_garbage () {
    stderr "${my_name} (PID=${BASHPID}) Collect garbage for session “${session_id}”"
    for file in /tmp/${my_name}_*; do
        rm -f "$file" 2>/dev/null
    done
}

render_qlr () {
    # 1: directory inside which we render the files (default to *, ie: all dirs in the SOXLOG directory)
    # Already converted files aren’t converted again
    for qlrf in "${QL_SOXLOG_DIRECTORY}"/${1:-*}/*.qlr; do
        [[ ! -f "${qlrf%.*}.flac" ]] && sed -e 's/ -t alsa / -p /g'  "${qlrf}" > "${qlrf%.*}.qlt"
    done
    for qlt in "${QL_SOXLOG_DIRECTORY}"/${1:-*}/*.qlt; do
         sox -q -t sox <(/bin/bash "${qlt}") -t flac -C1 "${qlt%.*}.flac"
         rm -vf "${qlt}"
    done || true
}

common_lib_loaded='true'

declare -r dirname="${QL_SOXLOG_DIRECTORY}/${PPID}${session_id}"
[[ -d "${dirname}" ]] || mkdir -p "${dirname}" 2>/dev/null || true

declare -gr soxlog_file="${dirname}/${EPOCHREALTIME}.${BASHPID}.qlr"

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    echo "Direct execution of a file meant to be sourced. Exiting." >&2; exit 2
fi
